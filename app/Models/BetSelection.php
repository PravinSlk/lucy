<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BetSelection extends Model
{
    use HasFactory;

    protected $table = 'bet_selections';

    protected $guarded = [];
}
