<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use App\Rules\CheckStakeAmount;
use App\Rules\CheckSelections;

use App\models\Player;
use App\models\BalanceTransaction;
use App\models\Bet;
use App\models\BetSelection;

class BetController extends Controller
{

    public function makeBet(Request $request)
    {
    	$status = 201;
    	$response = [];
    	$request_data = $request->json()->all();
		
		if(empty($request_data)){
			//input data structure should be valid
			$response['errors'][] = $this->errorMessage(1);
			$status = 400;
		}else{

			$validator = Validator::make($request_data, [
				'player_id' => ['required','integer'],
			    'stake_amount' => ['required','string',new CheckStakeAmount()],
			    'selections' => ['required','array',new CheckSelections()],
			],[
				'player_id.required' => ['code'=>0,'message'=>'The player id field is required.'],
				'player_id.exists' => ['code'=>0,'message'=>'The selected player id is invalid.'],
				'stake_amount.required' => ['code'=>0,'message'=>'The stake amount field is required.'],
				'stake_amount.string' => ['code'=>0,'message'=>'The stake amount must be a string.'],
				'selections.required' => ['code'=>0,'message'=>'The selections field is required.'],
				'selections.array' => ['code'=>0,'message'=>'The selections must be an array.'],
			]);


			if ($validator->fails()) {
				
				foreach ($validator->messages()->getMessages() as $field_name => $message){
					if($field_name != 'selections'){
						$response['errors'][] = $message[0];
					}else{
						if(is_array($message[0]['message']))
						$response['selections'] = $message[0]['message'];
						else $response['errors'][] = $message[0];
					}   
            	}

	        }else{


				$stake_amount = $request_data['stake_amount'];

		    	$selections = $request_data['selections'];

		    	$total_odds = [$stake_amount];
		    	foreach ($selections as $select) {
		    		$total_odds[] = $select['odds'];
		    	}

		    	$amount = array_product($total_odds);

		    	if($amount > 20000){
		    		$response['errors'][] = $this->errorMessage(9);
		    	}else{

		    		Player::updateOrCreate(['id'=>$request_data['player_id']]);

		    		$player = Player::where('id',$request_data['player_id'])->first();

		    		BalanceTransaction::create([
		    			'player_id'=> $player->id,
		    			'amount'=> $amount,
		    			'amount_before'=> $player->balance,
		    		]);

		    		$bet = Bet::create([
		    			'stake_amount'=> $stake_amount,
		    		]);

		    		$selectionData = [];
		    		foreach ($selections as $select) {
			    		$selectionData[] = [
			    			'bet_id' => $bet->id,
			    			'selection_id' => $select['id'],
			    			'odds' => $select['odds']
			    		];
			    	}

			    	BetSelection::insert($selectionData);

			    	$player->update(['balance'=>$player->balance-$amount]);
		    	}
		 
		    	

	        }

		}

		return $this->apiResponse($status,$response);
    }
}
