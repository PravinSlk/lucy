<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function errorMessage($code,$options = [])
    {	
    	$response = [];
    	$message = '';

    	switch ($code) {
		  case (0):
		    $message = "Unknown error";
		    break;
          case (1):
            $message = "Betslip structure mismatch";
            break;
          case (2):
            $message = "Minimum stake amount is 0.3";
            break;
          case (3):
            $message = "Maximum stake amount is 10000";
            break;
          case (4):
            $message = "Minimum number of selections is 1";
            break;
          case (5):
            $message = "Maximum number of selections is 20";
            break;
          case (6):
            $message = "Minimum odds are 1";
            break;
          case (7):
            $message = "Maximum odds are 10000";
            break;
          case (8):
            $message = "Duplicate selection found";
            break;
          case (9):
            $message = "Maximum win amount is 20000";
            break;
          case (10):
            $message = "Your previous action is not finished yet";
            break;
          case (11):
            $message = "Insufficient balance";
            break;
		  default:
		    $message =  "Unknown error";
		}
    	
    	$response['code'] = $code;
    	$response['message'] = $message;

    	return $response;
    }

    public function apiResponse($status,$response)
    {
    	return response($response,$status)->header('Content-Type', 'application/json');
    }
}

