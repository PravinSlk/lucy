<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class CheckStakeAmount implements Rule
{

    private $message;

    private $code;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($message=null,$code=0)
    {
         $this->message = $message;
         $this->code = $code;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $count = strlen(substr(strrchr($value, "."), 1));
        if($count > 2){
            $this->message = 'Max number of numbers after dot is 2';
            return false;
        }else{

            if($value < 0.3){
                $this->message = 'Minimum stake amount is 0.3';
                $this->code = 2;
                return false;
            }elseif($value > 10000){
                $this->message = 'Maximum stake amount is 10000';
                $this->code = 3;
                return false;
            }else{
               return true; 
            }
            
        }

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return [['code'=>$this->code,'message'=>$this->message]];
    }
}
