<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class CheckSelections implements Rule
{
    private $message;

    private $code;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($message=null,$code=0)
    {
         $this->message = $message;
         $this->code = $code;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if(is_array($value))
        $data = array_filter($value);
        else $data = [];
        if(count($data) < 1){
            $this->message = 'Minimum number of selections is 1';
            $this->code = 4;
            return false;

        }elseif(count($data) > 20){
            $this->message = 'Maximum number of selections is 20';
            $this->code = 5;
            return false;

        }else{

            $response = $ids = $duplicates = [];
            foreach ($value as $key => $row) {

                $errors = [];
                if(!isset($row['id']))
                {
                    $errors[] = ['code'=>0,'message'=>'The id field is required.'];
                }else{
                    
                    if(array_search($row['id'], $ids)){
                        $duplicates[$row['id']] = $row['id'];
                    }

                    $ids[$key+1] = $row['id'];
                }

                if(!isset($row['odds']))
                {
                    $errors[] = ['code'=>0,'message'=>'The odds field is required.'];
                }else{
                    if($row['odds'] < 1){
                        $errors[] = ['code'=>6,'message'=>'Minimum odds are 1'];
                    }elseif($row['odds'] > 10000){
                        $errors[] = ['code'=>7,'message'=>'Maximum odds are 10000'];
                    }
                }
                ///dd($errors);

                if(!empty($errors)){
                    $response[$key+1] = $row;
                    $response[$key+1]['errors'] = $errors;
                }
                
                # code...
            }

            foreach ($value as $k => $rowData) {
                if(isset($rowData['id']) && isset($duplicates[$rowData['id']])){
                    if(!isset($response[$k+1])){
                       $response[$k+1] = $rowData;
                    }

                     $response[$k+1]['errors'][] = ['code'=>8,'message'=>'Duplicate selection found'];
                }
            }

            if(!empty($response)){
                $this->message = $response;
                return false;
            }else{
                return true;
            }
            

            //dd($response,$ids,$duplicates);
        }

       
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return [['code'=>$this->code,'message'=>$this->message]];
    }
}
